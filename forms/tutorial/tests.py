import unittest

from pyramid import testing


class TutorialViewTests(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_wiki(self):
        from tutorial.views import WikiViews

        request = testing.DummyRequest()
        inst = WikiViews(request)
        response = inst.wiki_view()
        self.assertIn(dict(
            uid='100',
            title='Page 100',
            body='<em>100</em>'),
            response['pages'])


class TutorialFunctionalTests(unittest.TestCase):
    def setUp(self):
        from tutorial import main
        app = main({})
        from webtest import TestApp

        self.testapp = TestApp(app)

    def test_wiki(self):
        res = self.testapp.get('/', status=200)
        self.assertIn(b'<h1>Wiki', res.body)
